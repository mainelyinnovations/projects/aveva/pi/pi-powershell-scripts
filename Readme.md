# OSIsoft PI System PowerShell Scripts

This repository contains a collection of PowerShell scripts that can be used to perform various tasks related to the OSIsoft PI System. The scripts are organized into categories based on their functionality.

## Scripts

The repository currently contains the following script categories:

- Data Archive
- PI Web API

### Data Archive

- **Security**: scripts related to managing PI System security, including managing identities and trust relationships.
  - **Identity**: scripts for managing PI System identities, including creating, modifying, and deleting identities.
  - **Identity Mapping**: scripts for managing identity mappings, including creating and modifying mappings.
  - **Trust**: scripts for managing trust relationships, including creating and modifying trusts.
- **Values**: scripts for managing PI System values, including deleting values for PI Points for a given time range.
  - [Add Values From CSV](./DataArchive/Values/AddValuesFromCsv.ps1)
    - Add values from CSV to a Data Archive. Supports using buffering for collectives if run on a server with Buffering installed.
  - [Collective Delete All Values for Point](./DataArchive/Values/CollectiveDeleteAllValuesForPoints.ps1)
    - Delete all values for a list of point between the provided start & end time on the specified list of Data Archive servers.
  - [Delete All Values for Point](./DataArchive/Values/DeleteAllValuesForPoint.ps1)
    - Delete all values for a specific point between the provided start & end time.
  - [Delete All Values for Points](./DataArchive/Values/DeleteAllValuesForPoints.ps1)
    - Delete all values for a list of points between the provided start & end time.
  - [Delete Values for Point](./DataArchive/Values/DeleteValuesForPoint.ps1)
    - Delete all values for a specific point that matches a specific value between the provided start & end time.
  - [Delete Values for Points](./DataArchive/Values/DeleteValuesForPoints.ps1)
    - Delete all values for a list of points that matches a specific value between the provided start & end time.
  - [Delete Values for PointSource](./DataArchive/Values/DeleteValuesForPointSource.ps1)
    - Delete all values for all points within a specific point source that match a specific value between the provided start & end time and delete them.
  - [Get Values for Tag Filter](./DataArchive/Values/GetValuesForTagFilter.ps1)
    - Get all values between start & end time for points matching the specified tag filter.
- **Point**: scripts for managing PI Points, including creating, modifying, and updating the attribute values on a PI Point.
- **Collective**: scripts for managing PI Collectives, including creating and modifying collectives.
- **DigitalState**: scripts for managing digital states, including creating and modifying digital states.

### PI Web API

- **Metrics**: scripts for monitoring PI Web API metrics.
  - [Get User Number of Requests](./PIWebAPI//Metrics/GetUserNumRequests.ps1)
    - Gets the total request count and the number of requests each user has made against the specified PI Web API instance.
  - [Restart Service with Invalid Cache](./PIWebAPI//Metrics/RestartServiceInvalidCache.ps1)
    - Restarts the PI Web API if any connected entity has an invalid cache.

## Usage

To use these scripts, you will need to have the PI System PowerShell Tools installed on your computer. Please refer to the [official documentation](https://docs.aveva.com/bundle/pi-powershell/page/html/51ab160b-1bc3-43e2-810d-d45214fa4606.htm) for instructions on how to install these modules.

Once the modules are installed, you can use the scripts by running them in PowerShell. You may need to adjust the scripts to fit your specific needs.

## Notes

Please note that some of the scripts may require specific permissions or roles in order to run. Additionally, some scripts may make changes to your PI System, so it is recommended to test them in a non-production environment before using them in a production setting.

## Contribute

If you have any additional scripts that you would like to contribute, or if you find any bugs, please feel free to open a pull request or issue on the repository. We welcome any contributions that improve the functionality of these scripts or add new scripts to the collection.

## Additional Resources

- Maintained by [Mainely Innovations](https://mainely.io)
- [OSIsoft PowerShell Tools](https://docs.aveva.com/bundle/pi-powershell/page/html/51ab160b-1bc3-43e2-810d-d45214fa4606.htm)
- [PowerShell documentation](https://learn.microsoft.com/en-us/powershell/)

Please note that the scripts in this repository are provided as-is and are not officially supported by OSIsoft. Use them at your own risk and test them thoroughly in a non-production environment before using them in production.
