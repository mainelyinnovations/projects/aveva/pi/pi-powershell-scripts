######################################################################################
################### Get Number of User Requests for the PI Web API ###################
######################################################################################
# This script will get the number of total requests made on a PI Web API server and
# return the number of requests made by each user since the API was started.
#
# The parameters are specified in the "Variables" section. The rest
# of the code should be left untouched.
######################################################################################
######################################################################################

###########
# Variables
###########
$piwebapi = "my-webserver"

########################################
########################################
# Optional SSL Certificate Ignore
# This can be removed if necessary.
########################################
########################################
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

########################################
########################################
# Do not edit below this line
########################################
########################################
# Perform the request and parse the JSON response.
$url = "https://$($piwebapi)/piwebapi/system/metrics/requests"
$response = Invoke-WebRequest -Uri $url -UseDefaultCredentials
$json = ConvertFrom-Json $response.content

# API Request Info
$totalRequests = 0
$requestResults = @()
$users = @{}

# Loop through each API endpoint.
foreach ($endpoint in $json.Requests.PSObject.Properties) {
  # Loop through each method (POST, GET, etc) on the endpoint.
  foreach ($method in $endpoint.Value.Methods.PSObject.Properties) {
    # Add to the total number of requests.
    $totalRequests += $method.Value.Total

    # For each user in the method's request count, add them to the User hash map and update their total request count.
    foreach ($user in $method.Value.CountByUser.PSObject.Properties) {
      if ($users.Keys -Contains $user.Name) {
        $users[$user.Name] += $user.Value
      } else {
        $users[$user.Name] = $user.Value
      }
    }
  }
}

# Output the name of the environment & total number of requests.
Write-Output "Environment - $($piwebapi)"
Write-Output "Total Requests - $($totalRequests)"

# Create a table row for each user and add their percentage of the total number of requests.
foreach ($user in $users.Keys) {
  $requestResults += [PSCustomObject]@{
    User        = $user
    NumRequests = $users[$user]
    Percent     = ($users[$user] / $totalRequests).tostring("P")
  }
}

# Output the users and number of requests, ordered from most to least.
$requestResults | Sort-Object Percent, NumRequests -Descending | Format-Table
