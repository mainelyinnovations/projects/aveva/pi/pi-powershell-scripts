######################################################################################
################### Get Number of User Requests for the PI Web API ###################
######################################################################################
# This script will get check each of the PI Web API servers in the array variable for
# the status of the cache for each user. If the cache should have expired more than 15
# minutes in the past, then the script will attempt to restart the PI Web API instance
# on the given server. The script will also log all events to the file specified and
# cleanup the log files after the number of days specified.
#
# To use this, the user or service account running the script must be a member of the
# following local groups on each server:
# - Administrators
# - PI Web API Admins
#
# The parameters are specified in the "Variables" section. The rest
# of the code should be left untouched.
#
# Based on KB - https://customers.osisoft.com/s/knowledgearticle?knowledgeArticleUrl=1863925223089596597-380105
######################################################################################
######################################################################################

###########
# Variables
###########
$piwebapiServers = @("my-webserver")
$logFilePath = "C:\Temp\Logs\PIWebAPI\*"
$logCutoffDays = -7

########################################
########################################
# Optional SSL Certificate Ignore
# This can be removed if necessary.
########################################
########################################
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

########################################
########################################
# Do not edit below this line
########################################
########################################
$logFile = "$($logFilePath)\Cache-$(Get-Date -Format 'yyyy-MM-dd').log"
$maxInt32 = [int32]::MaxValue

"#######################################" >> $logFile
"#######################################" >> $logFile
Get-Date >> $logFile

# Remove old log files.
$logCutoff = (Get-Date).AddDays($logCutoffDays)
Get-ChildItem -Path $logFilePath -Include "*.log" -File | Where-Object { $_.LastWriteTime -lt $logCutoff } | Remove-Item –Force -Verbose *>&1 $logFile

# Cache Info
$servers = @()
$users = @()

# Loop through each server instance.
foreach ($server in $piwebapiServers) {
  $restartRequired = $false
  $restarted = $false

  try {
    # Perform the request and parse the JSON response.
    $url = "https://$($server)/piwebapi/system/cacheinstances"
    $response = Invoke-WebRequest -Uri $url -UseDefaultCredentials -UseBasicParsing
    $json = ConvertFrom-Json $response.content

    # Loop through each cache instance.
    foreach ($cacheInstance in $json.Items) {
      $userRestart = $false
      # Get the cache expiration Datetime and convert it to a Datetime object.
      $cacheExpires = $cacheInstance.ScheduledExpirationTime
      $timeDifference = (Get-Date) - [DateTime]$cacheExpires
      # Set the expiration interval to 15 minutes, per KB.
      $expireInterval = New-TimeSpan -Minutes 15

      # Check if the user's cache expiration is past the time it should have expired.
      if ($timeDifference -gt $expireInterval) {
        # Set the user & server WebAPI restart flags to true.
        $userRestart = $true
        $restartRequired = $true
      }

      # Set up the user row for the output table.
      $users += [PSCustomObject]@{
        Server                  = $server
        User                    = $cacheInstance.User
        LastRefreshTime         = $cacheInstance.LastRefreshTime
        WillRefreshAfter        = $cacheInstance.WillRefreshAfter
        ScheduledExpirationTime = $cacheExpires
        RequiresRestart         = $userRestart
        TimeDifference          = $timeDifference
      }
    }
  } catch {
    Write-Output $_ >> $logFile
  }

  # If a restart of the WebAPI is required,
  if ($restartRequired) {
    try {
      # Create a remote session against the server that needs to have the WebAPI restarted.
      # This will used the credentials of the user or service account running the script.
      $session = New-PSSession $server -SessionOption (New-PSSessionOption -IncludePortInSPN)
      Invoke-Command -Session $session -ScriptBlock { Restart-Service -Name "piwebapi" } >> $logFile
      $restarted = $true
    } catch {
      Write-Output $_ >> $logFile
    }

    # Cleanup the remote session.
    Get-PSSession | Remove-PSSession >> $logFile
  }

  # Add each server to the cache info.
  $servers += [PSCustomObject]@{
    Server          = $server
    RestartRequired = $restartRequired
    Restarted       = $restarted
  }
}

# Output the users that require a WebAPI instance to be restarted.
$users | Sort-Object Server, User | Format-Table Server, User, LastRefreshTime, WillRefreshAfter, ScheduledExpirationTime, RequiresRestart, TimeDifference | Out-File -Append -FilePath $logFile -Width $maxInt32

# Output the server WebAPI statuses.
$servers | Sort-Object Server | Format-Table >> $logFile

"#######################################" >> $logFile
"#######################################" >> $logFile
