######################################################################################
########################## Delete Values for a Point Source ##########################
######################################################################################
# This script will delete all archived values for all points within a given point
# source between the start and end times where the archived value is exactly equal to
# or where the archived value is like the desired value to delete.
#
# The parameters are specified in the "Variables" section. The rest
# of the code should be left untouched.
######################################################################################
######################################################################################

###########
# Variables
###########
# Timestamps need to be system formatted, and not PI timestamps like *-5m
$archive = "myArchive"
$pointSource = "MyPS"
$startTime = "9/14/2021 8:00:00 AM"
$endTime = "9/14/2021 8:00:00 PM"
$value = "State: 246(Set: 0)"
$valueEquals = 0
# valueEquals is used to determine if the value specified in $value must be exactly equal to
#   the archived value, or if a "like" clause will be used to compare the values.
#   0 - use like comparison
#   1 - use equals comparison

#############
# State Info
#############
# State values can be removed, but they cannot be referenced (looked up) through the state name.
# Instead, state values need to be looked up by the state value instead of the name.
# As a reference, to delete "I/O Timeout" events, the $value variable would look like this:
# $value = "State: 246(Set: 0)"
# Where 246 is the state value for I/O Timeout

########################################
########################################
# Do not edit below this line
########################################
########################################
# Connect to the specified Data Archive
try{
  $con = Connect-PIDataArchive -PIDataArchiveMachineName $archive -ErrorAction Stop
} catch {
  Write-Error "Error connecting to the Data Archive: $_"
}

# Get all PI Points within the specified point source
try {
  if ($con.Connected) {
    $points = Get-PIPoint -WhereClause "pointsource:=$($pointSource)" -Connection $con -ErrorAction Stop
  }
} catch {
  Write-Error "Error retrieving points from the Data Archive: $_"
}

if ($archiveValues.Count -eq 0) {
  Write-Warning "No archive values found for the specified time range."
}

# Loop through all the points to get their values and deleted the specified values in the time range
foreach ($point in $points) {
  # Get all values for the specified PI Point between the Start & End time
  $archiveValues = Get-PIValue -Connection $con -PointName $point.Point.Name -StartTime $startTime -EndTime $endTime

  # Loop through the values
  foreach ($archiveValue in $archiveValues) {
    # Determine if the value should be removed based on whether the comparison is exactly equal to, or like.
    $remove = if ($valueEquals -eq 1) { $archiveValue.Value -eq $value } else { $archiveValue.Value -like $value }

    if ($remove) {
      Remove-PIValue -Connection $con -Event $archiveValue
    }
  }
}

# Disconnect from the Data Archive
if ($con.Connected) {
  Disconnect-PIDataArchive -Connection $con
}
