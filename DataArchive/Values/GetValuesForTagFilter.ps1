######################################################################################
############################## Get Values for Tag Filter #############################
######################################################################################
# This script will retrieve all tag values between the start & end times using the tag
# filter specified in the variables section. All timestamped values will be written to
# a CSV file using the `Export-CSV` cmdlet.
#
# The parameters are specified in the "Variables" section. The rest
# of the code should be left untouched.
######################################################################################
######################################################################################

###########
# Variables
###########
# Timestamps need to be system formatted, and not PI timestamps like *-5m
$archive = "myArchive"
$tagFilter = "*my-tag-filter*"
$startTime = "9/14/2021 8:00:00 AM"
$endTime = "9/14/2021 8:00:00 PM"
$outputFile = "C:\path\to\file.csv"

########################################
########################################
# Do not edit below this line
########################################
########################################
# Connect to the specified Data Archive
try {
  $con = Connect-PIDataArchive -PIDataArchiveMachineName $archive
} catch {
  Write-Error "Error connecting to the Data Archive: $($_)"
}

# Get all values for the PI Points between the Start & End time
try {
  if ($con.Connected) {
    $points = Get-PIPoint -WhereClause "Tag:=$($tagFilter)" -Connection $con
    $archiveValues = @()

    foreach ($point in $points) {
      # Add each value to an array for export.
      $archiveValues += (Get-PIValue -Connection $con -PointName "$($point.Point.Name)" -StartTime $startTime -EndTime $endTime -ErrorAction Stop) | ForEach-Object {
        # Return a custom object that includes the PI Point name, TimeStamp, and Value.
        # The default returned object does not include the PI Point name.
        return [PsCustomObject]@{
          Point     = $point.Point.Name
          TimeStamp = $_.TimeStamp
          Value     = $_.Value
        }
      }
    }

    # Log the data to the output file.
    $archiveValues | Export-Csv -Path $outputFile -NoTypeInformation
  }
} catch {
  Write-Error "Error retrieving values from the Data Archive: $($_)"
}

# Disconnect from the Data Archive
if ($con.Connected) {
  Disconnect-PIDataArchive -Connection $con
}
