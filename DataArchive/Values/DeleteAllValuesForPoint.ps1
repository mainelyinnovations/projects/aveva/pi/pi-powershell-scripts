######################################################################################
##################### Delete All Values for a Multiple PI Points #####################
######################################################################################
# This script will delete all archived values for a PI Point between the start and
# end times where the archived value is exactly equal to or where the archived
# value is like the desired value to delete.
#
# The parameters are specified in the "Variables" section. The rest
# of the code should be left untouched.
######################################################################################
######################################################################################

###########
# Variables
###########
# Timestamps need to be system formatted, and not PI timestamps like *-5m
$archive = "myArchive"
$point = "myPointName"
$startTime = "9/14/2021 8:00:00 AM"
$endTime = "9/14/2021 8:00:00 PM"

########################################
########################################
# Do not edit below this line
########################################
########################################
# Connect to the specified Data Archive
try {
  $con = Connect-PIDataArchive -PIDataArchiveMachineName $archive
} catch {
  Write-Error "Error connecting to the Data Archive: $_"
}

# Remove all values for the specified PI Point between the Start & End time
try {
  if ($con.Connected) {
    Remove-PIValue -PointName $point -StartTime $startTime -EndTime $endTime -Connection $con
  }
} catch {
  Write-Error "Error removing values from the Data Archive: $_"
}

# Disconnect from the Data Archive
if ($con.Connected) {
  Disconnect-PIDataArchive -Connection $con
}
