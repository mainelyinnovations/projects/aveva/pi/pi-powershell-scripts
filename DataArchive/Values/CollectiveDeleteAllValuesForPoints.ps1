######################################################################################
############### [Collective] Delete All Values for a Multiple PI Points ##############
######################################################################################
# This script will delete all archived values for multiple PI Points in the list of
# Data Archive servers between the start and end times where the archived value is
# exactly equal to or where the archived value is like the desired value to delete.
#
# The parameters are specified in the "Variables" section. The rest
# of the code should be left untouched.
######################################################################################
######################################################################################

###########
# Variables
###########
# Timestamps need to be system formatted, and not PI timestamps like *-5m
$archives = @("myArchive")
$points = @("myPointName")
$startTime = "9/14/2021 8:00:00 AM"
$endTime = "9/14/2021 8:00:00 PM"

########################################
########################################
# Do not edit below this line
########################################
########################################
foreach ($archive in $archives) {
  try {
    # Connect to the specified Data Archive
    $con = Connect-PIDataArchive -PIDataArchiveMachineName $archive

    # Loop through all the points.
    foreach ($point in $points) {
      # Remove all values for the specified PI Point between the Start & End time
      try {
        if ($con.Connected) {
          Remove-PIValue -PointName $point -StartTime $startTime -EndTime $endTime -Connection $con
        }
      } catch {
        Write-Error "Error removing values from the Data Archive: $_"
      }
    }
  } catch {
    Write-Error "Error connecting to the Data Archive: $_"
  }

  # Disconnect from the Data Archive
  if ($con.Connected) {
    Disconnect-PIDataArchive -Connection $con
  }
}
