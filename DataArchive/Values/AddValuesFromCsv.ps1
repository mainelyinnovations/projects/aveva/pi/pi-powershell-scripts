######################################################################################
############################### Import Values from CSV ###############################
######################################################################################
# This script will import values from CSV into the designated archive while utilizing
# buffering. To support the use of buffering, the script must be run from a location
# that has buffering installed.
#
# The parameters are specified in the "Variables" section. The rest
# of the code should be left untouched.
######################################################################################
######################################################################################

###########
# Variables
###########
$archive = "myArchive"
$data = Import-Csv -Path ".\Processes.csv"

# Expects CSV in the following format:
# TagName,Timestamp,Value

########################################
########################################
# Do not edit below this line
########################################
########################################
try {
  # Connect to the specified Data Archive
  $con = Connect-PIDataArchive -PIDataArchiveMachineName $archive

  # Loop through the values
  foreach ($row in $data) {
    try {
      if ($con.Connected) {
        Add-PIValue -PointName $row[0] -Time $row[1] -Value $row[2] -WriteMode Append -Connection $con -Buffer
      }
    } catch {
      Write-Error "Error sending values to the Data Archive: $_"
    }
  }
} catch {
  Write-Error "Error connecting to the Data Archive: $_"
}

# Disconnect from the Data Archive
if ($con.Connected) {
  Disconnect-PIDataArchive -Connection $con
}
